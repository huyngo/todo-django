import csv

from datetime import date
from io import StringIO

import requests

from django.shortcuts import redirect, render
from django.urls import reverse

from todo_app.utils import (
    compare_time, is_before, is_today, is_after, summarize_list)

API_BASE = 'http://127.0.0.1:8000/api%s'


def view_all_lists(request):
    """View list of all todo lists."""
    endpoint = API_BASE % '/lists/'
    response = requests.get(endpoint)
    todo_lists = [summarize_list(todo_list) for todo_list in response.json()]
    context = {
        'past': filter(is_before, todo_lists),
        'today': filter(is_today, todo_lists),
        'future': filter(is_after, todo_lists)
    }
    return render(request, 'lists.html', context)


def view_list(request, list_id):
    """View one todo list."""
    endpoint = API_BASE % f'/lists/{list_id}/'
    response = requests.get(endpoint)
    context = response.json()
    context['past'] = is_before(context)

    if request.method == 'GET':
        return render(request, 'todo.html', context)
    if context['past']:
        return redirect('todo', list_id)
    try:
        checked = request.POST.copy().pop('todo')
        deleted = request.POST.copy().pop('delete')
    except KeyError:
        checked = []  # nothing is checked
        deleted = []
    for item in context['items']:
        if str(item['id']) in deleted:
            endpoint = API_BASE % f'/items/{item["id"]}/'
            requests.delete(endpoint)
        elif str(item['id']) in checked and not item['status']:
            endpoint = API_BASE % f'/items/{item["id"]}/'
            requests.patch(endpoint, json={'status': True})
        elif str(item['id']) not in checked and item['status']:
            endpoint = API_BASE % f'/items/{item["id"]}/'
            requests.patch(endpoint, json={'status': False})
    return redirect('todo', list_id)


def create_list(request):
    """Create a new todo list."""
    if request.method == 'GET':
        return render(request, 'create.html', {'type': 'list'})
    endpoint = API_BASE % '/lists/'
    valid_date = request.POST['valid_until']
    if compare_time(valid_date):
        valid_date = date.today().isoformat()
    response = requests.post(endpoint, json={
        'name': request.POST['name'],
        'valid_until': valid_date
    })
    list_id = response.json()['id']
    return redirect('todo', list_id)


def delete_list(request, list_id):
    """Delete a todo list."""
    if request.method == 'GET':
        return HttpResponse(
            'Method not allowed', status_code=HTTPStatus.METHOD_NOT_ALLOWED)
    endpoint = API_BASE % f'/lists/{list_id}'
    response = requests.delete(endpoint)
    return redirect('lists')


def create_item(request, list_id):
    """Create a new todo item."""
    if request.method == 'GET':
        return render(request, 'create.html', {'type': 'item'})
    endpoint = API_BASE % '/items/'
    response = requests.post(endpoint, json={
        'name': request.POST['name'],
        'parent': list_id
    })
    return redirect('todo', list_id)


def import_csv(request):
    """View for importing CSV."""
    if request.method == 'GET':
        return render(request, 'import.html')
    fname = request.FILES['file']
    f = fname.read().decode('utf-8')
    f = StringIO(f)
    reader = csv.reader(f, delimiter=',', lineterminator='\n')
    reader = list(reader)
    name, valid_date = tuple(reader.pop(0))
    print(name, valid_date)
    endpoint = API_BASE % '/lists/'
    response = requests.post(endpoint, json={
        'name': name,
        'valid_until': valid_date
    })
    list_id = response.json()['id']
    for row in reader:
        endpoint = API_BASE % '/items/'
        name, valid_date = tuple(row)
        response = requests.post(endpoint, json={
            'name': name,
            'parent': list_id
        })
    return redirect('todo', list_id)
