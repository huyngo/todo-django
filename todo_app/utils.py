from datetime import date


def compare_time(time):
    """Compare the date (from string format) with today.

    Returns

        -1 if the date is before today
        0 if it is the same date as today
        1 if the date is after today
    """
    time = date.fromisoformat(time)
    today = date.today()
    if time < today:
        return -1
    elif time > today:
        return 1
    else:
        return 0


def is_before(todo_list):
    """Wrapper for compare_time, returns whether the todo list is past."""
    return compare_time(todo_list['valid_until']) == -1


def is_today(todo_list):
    """Wrapper for compare_time, returns whether the todo list is today."""
    return compare_time(todo_list['valid_until']) == 0


def is_after(todo_list):
    """Wrapper for compare_time, returns whether the todo list is future."""
    return compare_time(todo_list['valid_until']) == 1


def summarize_list(todo_list):
    """Replace todo items with progress"""
    total = len(todo_list['items'])
    done = len(list(filter(lambda l: l['status'], todo_list['items'])))
    del todo_list['items']
    todo_list['total'] = total
    todo_list['done'] = done
    return todo_list
