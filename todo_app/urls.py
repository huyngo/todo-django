from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_all_lists, name='lists'),
    path('lists/create', views.create_list, name='create_list'),
    path('lists/<int:list_id>', views.view_list, name='todo'),
    path('lists/<int:list_id>/delete', views.delete_list, name='delete_list'),
    path('lists/<int:list_id>/create', views.create_item, name='create_item'),
    path('import', views.import_csv, name='import'),
]
