# Todo-list app

Yes, the infamous todo app.

## Dependencies

- Django
- djangorestframework

## Importing from CSV

You can import from CSV with first line indicating name and deadline date, and
subsequent lines indicating todo item name and status. For example:

```csv
Today list,2021-11-11
First item,1
Second item,0
Third item,0
```

## Copying

    Copyright © 2021 Ngô Ngọc Đức Huy
    This work is free. You can redistribute it and/or modify it under the
    terms of the Do What The Fuck You Want To Public License, Version 2,
    as published by Sam Hocevar. See COPYING or http://www.wtfpl.net/ for more
    details.
