from datetime import date

from django.db import models


class TodoList(models.Model):
    """The list of todo items."""
    name = models.CharField(max_length=31, default=date.today)
    valid_until = models.DateField(
        'date after which the list is invalid',
        default=date.today
    )


class TodoItem(models.Model):
    """An item in a todo list."""
    name = models.CharField(max_length=127)
    status = models.BooleanField('Whether the task is done', default=False)
    parent = models.ForeignKey(
        TodoList, related_name='items',
        on_delete=models.CASCADE)
