from rest_framework import routers

from todo_api import views

router = routers.DefaultRouter()
router.register(r'items', views.ItemViewSet)
router.register(r'lists', views.ListViewSet)

urlpatterns = router.urls
