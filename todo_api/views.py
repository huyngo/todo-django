from rest_framework import viewsets, permissions

from todo_api.models import TodoItem, TodoList
from todo_api.serializers import ItemSerializer, ListSerializer


class ItemViewSet(viewsets.ModelViewSet):
    """API endpoints for todo items."""
    queryset = TodoItem.objects.all()
    serializer_class = ItemSerializer


class ListViewSet(viewsets.ModelViewSet):
    """API endpoints for todo lists."""
    queryset = TodoList.objects.all()
    serializer_class = ListSerializer
