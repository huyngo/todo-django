from datetime import date

from rest_framework import serializers

from todo_api.models import TodoItem, TodoList


class ItemSerializer(serializers.ModelSerializer):
    """Serializer for a todo item."""
    class Meta:
        model = TodoItem
        fields = '__all__'


class ListSerializer(serializers.ModelSerializer):
    """Serializer for a todo list."""
    items = ItemSerializer(many=True, read_only=True)
    class Meta:
        model = TodoList
        fields = '__all__'
